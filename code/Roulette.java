import java.util.*;

public class Roulette {
    public static final Scanner scan = new Scanner(System.in);
    public static void main(String[] args)
    {
        RouletteWheel roulette = new RouletteWheel();
        double cash = 1000;
        double lost = 0;
        double won = 0;
		String response = "";
		System.out.println("Would you like to make a bet? y/n");
        response = validateAnswer();

        if (response.equals("y"))
        {
            response = "n";
            while (response.equals("n") && cash > 0)
            {
                System.out.println("What number would you like to bet on between 0 - 36 (inclusive)?");
                int betNum = Integer.parseInt(scan.nextLine());

                while (betNum > 36 || betNum < 0)
                {
                    System.out.println("Choose a number between 0-36:");
                    betNum = Integer.parseInt(scan.nextLine());
                }

                System.out.println("How much money would you like to place?");
                double amount = Double.parseDouble(scan.nextLine());

                while (amount > cash || amount <= 0)
                {
                    System.out.println("Amount must be greater than 0 and less than " + cash);
                    amount = Double.parseDouble(scan.nextLine());
                }

                roulette.spin();
                System.out.println("Number spun " + roulette.getValue());
                if (roulette.getValue() == betNum)
                {
                    System.out.println("You have won " + (amount*35) + "$");
                    cash = cash + (amount*35);
                    won = won + amount*35;
                }
                else
                {
                    System.out.println("You have lost " + (amount) + "$");
                    cash = cash - amount;
                    lost = lost + amount;
                }
                System.out.println("Money left: " + cash + "$");
                System.out.println("Do you want to quit? y/n");
                response = validateAnswer();

                if (cash == 0)
                {
                    System.out.println("It seems you have run out of money!");
                }
            }

            System.out.println("Amount won: " + won + "$. Amount lost: " + lost + "$");
        }
    }

    public static String validateAnswer()
    {
        String response = scan.nextLine();
        while (!response.equals("y") && !response.equals("n"))
        {
            System.out.println("Please answer y or n");
            response = scan.nextLine();
        }
        return(response);
    }
}

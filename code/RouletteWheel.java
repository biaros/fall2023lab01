import java.util.Random;
public class RouletteWheel {
    private Random ran;
    private int number;
    public RouletteWheel()
    {
        ran = new Random();
        number = 0;
    }

    public void spin()
    {
        this.number = ran.nextInt(37);
    }

    public int getValue()
    {
        return(this.number);
    }
}
